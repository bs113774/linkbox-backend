package me.sobolewski.linkbox.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class UUIDUtilsTest {

    private UUIDUtils uuidUtils;

    @BeforeEach
    void init(){
        uuidUtils = new UUIDUtils();
    }

    @Test
    void isValidUUID_true() {
        assertAll(
                () -> assertTrue(uuidUtils.isValidUUID(UUID.randomUUID().toString())),
                () -> assertTrue(uuidUtils.isValidUUID("00000000-aaaa-ffff-9999-1234567890ab"))
        );

    }

    @Test
    void isValidUUID_false() {
        assertAll(
                () -> assertFalse(uuidUtils.isValidUUID(null)),
                () -> assertFalse(uuidUtils.isValidUUID(String.valueOf(true))),
                () -> assertFalse(uuidUtils.isValidUUID("")),
                () -> assertFalse(uuidUtils.isValidUUID("00000000-aaaa-zzzz-9999-1234567890ab")),
                () -> assertFalse(uuidUtils.isValidUUID("00000000-aaaa-ffff-9999-1234567890a"))
        );
    }
}