package me.sobolewski.linkbox.repository;

import me.sobolewski.linkbox.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

  /**
   * Find a user by email, and return an Optional that contains the user if found,
   * or an empty Optional if not found.
   *
   * @param email The email of the user to find.
   * @return Optional<User>
   */
  Optional<User> findByEmail(String email);

  /**
   * Check if a user exists with the given email.
   *
   * @param email The email to check.
   * @return A boolean value.
   */
  boolean existsByEmail(String email);

}
