package me.sobolewski.linkbox.repository;

import me.sobolewski.linkbox.model.Link;
import me.sobolewski.linkbox.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface LinkRepository extends PagingAndSortingRepository<Link, Long> {

  /**
   * Find all links that have a given owner.
   *
   * @param owner The user who owns the links.
   * @return A list of links
   */
  Page<Link> findAllByOwner(User owner, Pageable pageable);

  Page<Link> findAllByOwnerAndFavoriteAndArchived(User owner, Boolean favorite, Boolean archived, Pageable pageable);

  Page<Link> findAllByOwnerAndArchived(User owner, Boolean archived, Pageable pageable);

  Integer countAllByOwner(User owner);
}
