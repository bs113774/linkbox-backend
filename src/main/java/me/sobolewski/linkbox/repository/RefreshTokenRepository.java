package me.sobolewski.linkbox.repository;

import me.sobolewski.linkbox.model.RefreshToken;
import me.sobolewski.linkbox.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {

  Optional<RefreshToken> findByToken(String token);

  boolean existsByUser(User user);

  void deleteByUser(User user);

}
