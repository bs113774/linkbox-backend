package me.sobolewski.linkbox.repository;

import me.sobolewski.linkbox.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserProfileRepository extends JpaRepository<UserProfile, UUID> {

  /**
   * Find a user profile by its UUID,
   * and return an Optional that contains the user profile if found,
   * or an empty Optional if not found.
   *
   * @param uuid The UUID of the user profile.
   * @return Optional<UserProfile>
   */
  Optional<UserProfile> findByUuid(UUID uuid);

  /**
   * Find a user profile by alias,
   * and return an Optional that contains the user profile if found,
   * or an empty Optional if not found.
   *
   * @param alias The alias of the user profile to find.
   * @return Optional<UserProfile>
   */
  Optional<UserProfile> findByAlias(String alias);

  /**
   * Returns true if there is a user with the given alias.
   *
   * @param alias The alias of the user.
   * @return A boolean value.
   */
  boolean existsByAlias(String alias);

  boolean existsByUuidOrAlias(String uuid, String alias);

}
