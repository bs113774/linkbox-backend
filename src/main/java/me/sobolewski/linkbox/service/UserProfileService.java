package me.sobolewski.linkbox.service;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.exception.UserProfileNotFoundException;
import me.sobolewski.linkbox.model.User;
import me.sobolewski.linkbox.model.UserProfile;
import me.sobolewski.linkbox.payload.request.UserProfileIdChangeRequest;
import me.sobolewski.linkbox.payload.response.MessageResponse;
import me.sobolewski.linkbox.repository.UserProfileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserProfileService {

  private static final String PROFILE_NOT_FOUND_MSG = "Taki profil nie istnieje";

  private final UserProfileRepository userProfileRepository;

  public UserProfile getUserProfileByUUID(UUID uuid) {
    return userProfileRepository.findByUuid(uuid)
        .orElseThrow(() -> new UserProfileNotFoundException(PROFILE_NOT_FOUND_MSG));
  }

  public UserProfile getUserProfileByAlias(String alias) {
    return userProfileRepository.findByAlias(alias)
        .orElseThrow(() -> new UserProfileNotFoundException(PROFILE_NOT_FOUND_MSG));
  }

  public ResponseEntity<MessageResponse> updateUserProfileId(User user, UserProfileIdChangeRequest request){
    UserProfile userProfile = user.getUserProfile();
    userProfile.setAlias(request.newId());

    try{
      userProfileRepository.save(userProfile);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(new MessageResponse("Wystąpił błąd. Spróbuj ponownie."));
    }
    return ResponseEntity.ok(new MessageResponse("Identyfikator konta został zmieniony!"));

  }

}
