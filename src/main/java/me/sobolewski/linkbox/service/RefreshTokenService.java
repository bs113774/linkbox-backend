package me.sobolewski.linkbox.service;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.exception.RefreshTokenException;
import me.sobolewski.linkbox.model.RefreshToken;
import me.sobolewski.linkbox.model.User;
import me.sobolewski.linkbox.repository.RefreshTokenRepository;
import me.sobolewski.linkbox.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RefreshTokenService {

  @Value("${linkbox.jwtRefreshExpirationMs}")
  private Long jwtRefreshExpirationMs;

  private final RefreshTokenRepository refreshTokenRepository;

  private final UserRepository userRepository;

  public Optional<RefreshToken> getByToken(String token) {
    return refreshTokenRepository.findByToken(token);
  }

  @Transactional
  public RefreshToken createRefreshToken(User user) {

    if (refreshTokenRepository.existsByUser(user)) {
      refreshTokenRepository.deleteByUser(user);
    }

    RefreshToken refreshToken = new RefreshToken();

    refreshToken.setUser(user);
    refreshToken.setExpiryDate(Instant.now().plusMillis(jwtRefreshExpirationMs));
    refreshToken.setToken(UUID.randomUUID().toString());

    refreshToken = refreshTokenRepository.save(refreshToken);
    return refreshToken;
  }

  public RefreshToken verifyExpiration(RefreshToken token) {
    if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
      refreshTokenRepository.delete(token);
      throw new RefreshTokenException(token.getToken(), "Refresh token was expired. Please make a new signin request");
    }

    return token;
  }

}
