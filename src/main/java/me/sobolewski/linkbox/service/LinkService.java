package me.sobolewski.linkbox.service;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.exception.LinkNotFoundException;
import me.sobolewski.linkbox.model.Link;
import me.sobolewski.linkbox.model.User;
import me.sobolewski.linkbox.model.UserProfile;
import me.sobolewski.linkbox.payload.request.LinkDescriptionRequest;
import me.sobolewski.linkbox.repository.LinkRepository;
import me.sobolewski.linkbox.repository.UserProfileRepository;
import me.sobolewski.linkbox.util.ReadUtils;
import me.sobolewski.linkbox.util.spec.LinkType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LinkService {

  private static final String LINK_NOT_FOUND_MSG = "Taki link nie istnieje";

  private final LinkRepository linkRepository;

  private final UserProfileRepository userProfileRepository;

  private final ReadUtils readUtils;

  public Link saveLink(Link link, User owner, LinkDescriptionRequest request) {
    link.setOwner(owner);
    link.setTitle(request.title());
    link.setDescription(request.description());
    return linkRepository.save(link);
  }

  public void shareLink(Link link, UserProfile userProfile) {
    userProfile.getLinks().add(link);
    userProfileRepository.save(userProfile);
  }

  public void deleteLink(Link link) {
    linkRepository.delete(link);
  }

  public Page<Link> getLinksByUser(User user, LinkType linkType, Integer page, String order) {
    Sort sort = "oldest".equalsIgnoreCase(order) ? Sort.by("addedAt").ascending() : Sort.by("addedAt").descending();
    Pageable pageable = PageRequest.of((page != null && page > 0) ? page - 1 : 0, 10, sort);
    if (linkType == LinkType.FAVORITES) {
      return linkRepository.findAllByOwnerAndFavoriteAndArchived(user, true, false, pageable);
    } else if (linkType == LinkType.ARCHIVED) {
      return linkRepository.findAllByOwnerAndArchived(user, true, pageable);
    }
    return linkRepository.findAllByOwnerAndArchived(user, false, pageable);
  }

  public Link getById(Long id) {
    return linkRepository.findById(id)
        .orElseThrow(() -> new LinkNotFoundException(LINK_NOT_FOUND_MSG));
  }

  public Integer getLinksCountByUser(User user) {
    return linkRepository.countAllByOwner(user);
  }
}
