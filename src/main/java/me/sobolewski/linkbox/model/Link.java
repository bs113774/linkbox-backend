package me.sobolewski.linkbox.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Link implements Serializable {
  public static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String url;
  private String title;
  private String description;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  private List<Tag> tags;

  private Boolean favorite = false;
  private Boolean archived = false;
  private Boolean unread = true;
  private LocalDateTime addedAt;

  @ManyToOne
  @JsonIgnore
  @ToString.Exclude
  private User owner;

  public Link(String url) {
    this.url = url;
  }

}
