package me.sobolewski.linkbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class LinkBoxApplication {

  public static void main(String[] args) {
    SpringApplication.run(LinkBoxApplication.class, args);
  }

}
