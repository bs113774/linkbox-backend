package me.sobolewski.linkbox.payload.response;

public record RefreshTokenResponse(String token, String refreshToken) {
}
