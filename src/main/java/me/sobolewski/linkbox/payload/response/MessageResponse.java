package me.sobolewski.linkbox.payload.response;

public record MessageResponse(String message) {
}
