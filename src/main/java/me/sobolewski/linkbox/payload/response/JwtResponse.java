package me.sobolewski.linkbox.payload.response;

import me.sobolewski.linkbox.model.enums.Role;

public record JwtResponse(String token, Long id, String email, Role role, String userProfile, String userProfileAlias) {
}
