package me.sobolewski.linkbox.payload.request;

public record PasswordChangeRequest(String newPassword, String oldPassword) {
}
