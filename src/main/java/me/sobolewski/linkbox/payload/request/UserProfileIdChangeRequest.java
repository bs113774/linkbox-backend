package me.sobolewski.linkbox.payload.request;

public record UserProfileIdChangeRequest(String oldId, String newId, String password) {
}
