package me.sobolewski.linkbox.payload.request;

public record EmailChangeRequest(String newEmail, String password) {
}
