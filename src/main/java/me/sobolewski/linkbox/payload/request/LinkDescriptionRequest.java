package me.sobolewski.linkbox.payload.request;

public record LinkDescriptionRequest(String title, String description) {
}
