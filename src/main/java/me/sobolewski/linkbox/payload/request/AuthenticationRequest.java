package me.sobolewski.linkbox.payload.request;

public record AuthenticationRequest(String email, String password) {
}
