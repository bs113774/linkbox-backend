package me.sobolewski.linkbox.payload.request;

public record RefreshTokenRequest(String refreshToken) {
}
