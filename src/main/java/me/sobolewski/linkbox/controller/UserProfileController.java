package me.sobolewski.linkbox.controller;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.model.User;
import me.sobolewski.linkbox.model.UserProfile;
import me.sobolewski.linkbox.payload.request.UserProfileIdChangeRequest;
import me.sobolewski.linkbox.payload.response.MessageResponse;
import me.sobolewski.linkbox.repository.UserProfileRepository;
import me.sobolewski.linkbox.service.UserProfileService;
import me.sobolewski.linkbox.util.UUIDUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api")
@RequiredArgsConstructor
public class UserProfileController {

  private final UserProfileService userProfileService;
  private final UserProfileRepository userProfileRepository;

  private final UUIDUtils uuidUtils;

  private final PasswordEncoder passwordEncoder;

  @GetMapping("/profile/{id}")
  public ResponseEntity<Object> getUserProfileById(@PathVariable String id) {
    if (uuidUtils.isValidUUID(id)) {
      if (!userProfileRepository.existsById(UUID.fromString(id))) {
        return ResponseEntity.notFound().build();
      }
      return ResponseEntity.ok(userProfileService.getUserProfileByUUID(UUID.fromString(id)));
    }
    if (!userProfileRepository.existsByAlias(id)) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(userProfileService.getUserProfileByAlias(id));
  }

  @GetMapping("/profile")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Object> getUserProfile(@AuthenticationPrincipal User user) {
    if (user.getUserProfile() == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(user.getUserProfile());
  }

  @PostMapping("/profile")
  public ResponseEntity<MessageResponse> updateUserProfileId(@AuthenticationPrincipal User user, @RequestBody UserProfileIdChangeRequest request) {
    if(!passwordEncoder.matches(request.password(), user.getPassword())){
      return new ResponseEntity<>(new MessageResponse("Podane aktualne hasło jest niepoprawne."), HttpStatus.BAD_REQUEST);
    }
    if(user.getUserProfile().getAlias() != null && user.getUserProfile().getAlias().equals(request.newId())){
      return ResponseEntity.badRequest().body(new MessageResponse("Nowy identyfikator profilu nie może być taki sam jak aktualny."));
    }
    if (userProfileRepository.existsByAlias(request.newId())) {
      return ResponseEntity.badRequest().body(new MessageResponse("Taki identyfikator profilu jest już zajęty."));
    }
    return userProfileService.updateUserProfileId(user, request);
  }

}
