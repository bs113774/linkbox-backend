package me.sobolewski.linkbox.controller;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.exception.RefreshTokenException;
import me.sobolewski.linkbox.model.RefreshToken;
import me.sobolewski.linkbox.model.User;
import me.sobolewski.linkbox.model.enums.Role;
import me.sobolewski.linkbox.payload.request.AuthenticationRequest;
import me.sobolewski.linkbox.payload.request.EmailChangeRequest;
import me.sobolewski.linkbox.payload.request.PasswordChangeRequest;
import me.sobolewski.linkbox.payload.request.RefreshTokenRequest;
import me.sobolewski.linkbox.payload.response.JwtResponse;
import me.sobolewski.linkbox.payload.response.MessageResponse;
import me.sobolewski.linkbox.payload.response.RefreshTokenResponse;
import me.sobolewski.linkbox.repository.UserRepository;
import me.sobolewski.linkbox.security.jwt.JwtUtils;
import me.sobolewski.linkbox.service.RefreshTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

  private final AuthenticationManager authenticationManager;
  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final JwtUtils jwtUtils;

  private final RefreshTokenService refreshTokenService;

  @PostMapping("/login")
  public ResponseEntity<Object> authenticateUser(@RequestBody AuthenticationRequest request) {
    Authentication authentication;
    try {
      authentication = authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(request.email(), request.password()));
    } catch (AuthenticationException e) {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new MessageResponse("Nieprawidłowy email lub hasło"));
    }

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String token = jwtUtils.generateJwtToken(authentication);
    //String refreshToken = refreshTokenService.createRefreshToken((User) authentication.getPrincipal()).getToken();

    User userPrincipal = (User) authentication.getPrincipal();

    return ResponseEntity.ok(new JwtResponse(
        token,
        userPrincipal.getId(),
        userPrincipal.getEmail(),
        userPrincipal.getRole(),
        userPrincipal.getUserProfile().getUuid().toString(),
        userPrincipal.getUserProfile().getAlias()));
  }

  @PostMapping("/register")
  public ResponseEntity<Object> registerUser(@RequestBody AuthenticationRequest request) {
    if (userRepository.existsByEmail(request.email())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Ten email jest już zajęty."));
    }

    User user = new User(
        request.email(),
        passwordEncoder.encode(request.password())
    );

    user.setRole(Role.ROLE_USER);
    userRepository.save(user);

    return ResponseEntity.ok(true);
  }

  @PostMapping("/change-password")
  public ResponseEntity<Object> changePassword(@AuthenticationPrincipal User user, @RequestBody PasswordChangeRequest request) {
    if(!passwordEncoder.matches(request.oldPassword(), user.getPassword())){
      return new ResponseEntity<>(new MessageResponse("Podane aktualne hasło jest niepoprawne."), HttpStatus.BAD_REQUEST);
    }

    user.setPassword(passwordEncoder.encode(request.newPassword()));

    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse("Hasło zostało zmienione!"));
  }

  @PostMapping("/change-email")
  public ResponseEntity<Object> changeEmail(@AuthenticationPrincipal User user, @RequestBody EmailChangeRequest request) {
    if(!passwordEncoder.matches(request.password(), user.getPassword())){
      return new ResponseEntity<>(new MessageResponse("Podane aktualne hasło jest niepoprawne."), HttpStatus.BAD_REQUEST);
    }

    if (user.getEmail().equals(request.newEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Nowy email nie może być taki sam jak aktualny."));
    }

    if (userRepository.existsByEmail(request.newEmail())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Ten email jest już zajęty."));
    }

    user.setEmail(request.newEmail());

    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse("Email został zmieniony!"));
  }

  @PostMapping("/authorize")
  public ResponseEntity<Object> authorizeToken(@AuthenticationPrincipal User user) {
    if(user != null){
      return ResponseEntity.status(HttpStatus.OK).build();
    }
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

  }

  @PostMapping("/refreshtoken")
  public ResponseEntity<RefreshTokenResponse> refreshToken(@RequestBody RefreshTokenRequest request) {
    String requestRefreshToken = request.refreshToken();

    return refreshTokenService.getByToken(requestRefreshToken)
        .map(refreshTokenService::verifyExpiration)
        .map(RefreshToken::getUser)
        .map(user -> {
          String token = jwtUtils.generateJwtTokenByUsername(user.getUsername());
          String refreshToken = refreshTokenService.createRefreshToken(user).getToken();
          return ResponseEntity.ok(new RefreshTokenResponse(token, refreshToken));
        })
        .orElseThrow(() -> new RefreshTokenException(requestRefreshToken,
            "Refresh token is not in database!"));
  }

}
