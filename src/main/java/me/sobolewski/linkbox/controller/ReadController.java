package me.sobolewski.linkbox.controller;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.service.LinkService;
import me.sobolewski.linkbox.util.ReadUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@RestController
@CrossOrigin
@RequestMapping("/api/read")
@RequiredArgsConstructor
public class ReadController {

  private final LinkService linkService;

  private final ReadUtils readUtils;

  @GetMapping()
  public ResponseEntity<String> getSourceCodeByLinkId(@RequestParam(required = false) Long id, @RequestParam(required = false) String url) {
    try {
      return ResponseEntity.ok(readUtils.getSourceCode(id != null ? linkService.getById(id).getUrl() : URLDecoder.decode(url, StandardCharsets.UTF_8)));
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }

  }

}
