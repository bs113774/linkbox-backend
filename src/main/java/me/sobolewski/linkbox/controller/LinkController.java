package me.sobolewski.linkbox.controller;

import lombok.RequiredArgsConstructor;
import me.sobolewski.linkbox.exception.LinkNotFoundException;
import me.sobolewski.linkbox.model.Link;
import me.sobolewski.linkbox.model.Tag;
import me.sobolewski.linkbox.model.User;
import me.sobolewski.linkbox.payload.request.LinkDescriptionRequest;
import me.sobolewski.linkbox.payload.response.MessageResponse;
import me.sobolewski.linkbox.repository.LinkRepository;
import me.sobolewski.linkbox.repository.TagRepository;
import me.sobolewski.linkbox.repository.UserProfileRepository;
import me.sobolewski.linkbox.service.LinkService;
import me.sobolewski.linkbox.util.spec.LinkType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api")
@RequiredArgsConstructor
public class LinkController {

  private static final String NO_SUCH_LINK_MSG = "Nie ma takiego linku";

  private final LinkRepository linkRepository;

  private final LinkService linkService;

  private final UserProfileRepository userProfileRepository;

  private final TagRepository tagRepository;

  @GetMapping("/links")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Object> getLinks(@AuthenticationPrincipal User user,
                                         @RequestParam(required = false) LinkType linkType,
                                         @RequestParam(required = false) Integer page,
                                         @RequestParam(required = false) String sort) {
    return ResponseEntity.ok(linkService.getLinksByUser(user, linkType, page, sort));
  }

  @GetMapping("/links/count")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Integer> getLinksCount(@AuthenticationPrincipal User user) {
    return ResponseEntity.ok(linkService.getLinksCountByUser(user));
  }

  @PostMapping("/links")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Object> addLink(@AuthenticationPrincipal User user, @RequestParam String url, @RequestBody LinkDescriptionRequest descriptionRequest) {
    Link link = new Link(URLDecoder.decode(url, StandardCharsets.UTF_8));
    link.setAddedAt(LocalDateTime.now());
    return ResponseEntity.ok(linkService.saveLink(link, user, descriptionRequest));
  }

  @PostMapping("/links/{id}/share")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public void shareLink(@AuthenticationPrincipal User user, @PathVariable Long id) {
    Link link = linkService.getById(id);
    linkService.shareLink(link, user.getUserProfile());
  }

  @DeleteMapping("/links/{id}/share")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public void deleteSharedLink(@AuthenticationPrincipal User user, @PathVariable Long id) {
    Link link = user.getUserProfile().getLinks().stream().filter(l -> l.getId().longValue() == id.longValue()).findAny().orElseThrow(() -> new LinkNotFoundException(NO_SUCH_LINK_MSG));
    if (link != null) {
      user.getUserProfile().getLinks().remove(link);
      userProfileRepository.save(user.getUserProfile());
    }
  }

  // Probably useless
  @GetMapping("/links/{id}")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Object> getLink(@PathVariable Long id) {
    if (!linkRepository.existsById(id)) {
      return ResponseEntity.badRequest().body(new MessageResponse(NO_SUCH_LINK_MSG));
    }
    return ResponseEntity.ok(linkService.getById(id));
  }

  @DeleteMapping("/links/{id}")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Object> deleteLink(@PathVariable Long id) {
    if (!linkRepository.existsById(id)) {
      return ResponseEntity.badRequest().body(new MessageResponse(NO_SUCH_LINK_MSG));
    }
    linkService.deleteLink(linkService.getById(id));
    return ResponseEntity.ok().build();
  }

  @PatchMapping("/links/{id}")
  @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
  public ResponseEntity<Object> updateLink(@PathVariable Long id, @RequestParam(required = false) String toggle, @RequestBody(required = false) List<Tag> tags) {
    if (!linkRepository.existsById(id)) {
      return ResponseEntity.badRequest().body(new MessageResponse(NO_SUCH_LINK_MSG));
    }
    Link link = linkService.getById(id);
    if (toggle != null) {
      switch (toggle) {
        case "unread" -> link.setUnread(!link.getUnread());
        case "favorite" -> link.setFavorite(!link.getFavorite());
        case "archive" -> link.setArchived(!link.getArchived());
      }
    }

    if(tags != null){
      List<Tag> mappedTags = tags.stream().map(tag -> tagRepository.findByName(tag.getName()).orElse(tag)).toList();

      link.setTags(new ArrayList<>(mappedTags));
    }

    linkRepository.save(link);
    return ResponseEntity.ok().build();
  }
}
