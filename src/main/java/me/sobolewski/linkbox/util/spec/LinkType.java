package me.sobolewski.linkbox.util.spec;

public enum LinkType {
  DEFAULT,
  FAVORITES,
  ARCHIVED
}
