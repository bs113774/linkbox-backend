package me.sobolewski.linkbox.util;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

@Component
public class ReadUtils {

  private static final Pattern RELATIVE_URL_REGEX = Pattern.compile("^[^\\/]+\\/[^\\/].*$|^\\/[^\\/].*$");

  /**
   * It takes a URL as a string, opens a connection to that URL, reads the source code of the page, and returns the
   * source code as a string
   *
   * @param urlString The URL of the website you want to get the source code from.
   * @return The source code of the website as String.
   */
  public String getSourceCode(String urlString) {
    StringBuilder sourceCode = new StringBuilder();
    try {
      URL url = new URL(urlString);

      HttpURLConnection connection = (HttpURLConnection) url.openConnection();

      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));

      for (String s : bufferedReader.lines().toList()) {
        sourceCode.append(s);
        sourceCode.append("\n");
      }

      bufferedReader.close();
      connection.disconnect();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return sourceCode.toString();
  }
}
