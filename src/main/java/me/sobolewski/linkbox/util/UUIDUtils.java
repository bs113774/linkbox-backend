package me.sobolewski.linkbox.util;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class UUIDUtils {

  private final static Pattern UUID_REGEX_PATTERN =
      Pattern.compile("[a-fA-F\\d]{8}-[a-fA-F\\d]{4}-[a-fA-F\\d]{4}-[a-fA-F\\d]{4}-[a-fA-F\\d]{12}");

  /**
   * Return true if the UUID matches the UUID regex pattern
   *
   * @param uuid The UUID to validate.
   * @return A boolean value.
   */
  public boolean isValidUUID(String uuid) {
    if (uuid == null) return false;
    return UUID_REGEX_PATTERN.matcher(uuid).matches();
  }

}
